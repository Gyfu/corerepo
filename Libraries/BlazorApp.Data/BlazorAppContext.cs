﻿using System;
using BlazorApp.Data.Domain;
using BlazorApp.Data.Domain.System;
using BlazorApp.Data.Domain.User;
using Microsoft.EntityFrameworkCore;

namespace BlazorApp.Data
{
    public class BlazorAppContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Log> Logs { get; set; }
        public BlazorAppContext(DbContextOptions options) : base(options)
        {

        }
    }
}
