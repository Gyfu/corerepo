﻿using System;
using System.Collections.Generic;
using System.Linq;
using BlazorApp.Core;
using Microsoft.EntityFrameworkCore;

namespace BlazorApp.Data
{
    /// <summary>
    /// Entity Framework repository
    /// </summary>
    public partial class EfRepository<T> : IRepository<T> where T : BaseEntity, new()
    {
        private readonly DbContext _context;
        private readonly DbSet<T> _entities;

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="context">Object context</param>
        public EfRepository(DbContext context)
        {
            this._context = context;
            this._entities = context.Set<T>();
        }

        public T GetById(object id)
        {
            return this._entities.Find(id);
        }

        public void Insert(T entity)
        {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                this._entities.Add(entity);

                this._context.SaveChanges();
        }

        public void Insert(IEnumerable<T> entities)
        {
                if (entities == null)
                    throw new ArgumentNullException("entities");
                foreach (var entity in entities)
                {
                    this._entities.Add(entity);
                }
                this._context.SaveChanges();
        }

        public void Update(T entity)
        {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                //if (!this._context.IsAttached(entity))
                //    this._entities.Attach(entity);

                //((DbContext)_context).Entry(entity).State = EntityState.Modified;
                this._context.SaveChanges();
        }

        /// <summary>
        /// Добавляет в контекст новую запись если ID==0 и обновляет существующую. При этом запись может быть не присоединена к контексту
        /// </summary>
        public void UpdateAndAttach(T entity, bool delete = false)
        {
                if (entity.Id != 0)
                {

                    var context = ((DbContext)_context);
                    var existEntity = this._entities.Find(entity.Id);
                    if(!delete)
                        context.Entry(existEntity).CurrentValues.SetValues(entity);

                    context.Entry(existEntity).State = !delete ? EntityState.Modified : EntityState.Deleted;
                    this._context.SaveChanges();
                }
                else if (!delete)
                {
                    var dbEntity = GetDbModel(entity);
                    this._entities.Add(dbEntity);
                    this._context.SaveChanges();
                    entity.Id = dbEntity.Id;
                    //  this._entities.Attach(GetDbModel(entity));
                    // ((DbContext)_context).Entry(entity).State = EntityState.Added;
                } 
        }

        public T GetDbModel(T enity)
        {
            var model = new T();
            var itemType = typeof(T);
            var props = itemType.GetProperties();


            foreach (var prop in props)
            {
                if (prop.CanWrite)
                {
                    var value = prop.GetValue(enity);

                    var underlineType = Nullable.GetUnderlyingType(prop.PropertyType);

                    if (value != null && (!prop.PropertyType.IsClass || prop.PropertyType == typeof(string)))
                    {
                        prop.SetValue(model, value);

                    }
                }

            }
            return model;
        }

        /// <summary>
        /// Добавляет в контекст новую запись из списка если ID==0 и обновляет существующую. При этом запись может быть не присоединена к контексту. НЕ ВСТАВЛЯЕТ ДОЧЕРНИЕ ОБъекты
        /// Сохранение вызывается после обновления всех записей. Изменения могут быть занесены в  лог.
        /// </summary>
        /// <param name="entities"></param>
        public void UpdateAndAttach(IEnumerable<T> entities, bool delete = false)
        {
                var ids = entities.Where(x => x.Id != 0).Select(x => x.Id);
                //Add to Cache 
                var list = _entities.Where(x => ids.Contains(x.Id)).ToList();
                foreach (var entity in entities)
                {

                    if (entity.Id != 0)
                    {

                        var context = ((DbContext)_context);
                        var existEntity = this._entities.Find(entity.Id);
                        context.Entry(existEntity).CurrentValues.SetValues(entity);
                        context.Entry(existEntity).State = !delete ? EntityState.Modified : EntityState.Deleted;
                    }
                    else if (!delete)
                    {
                        this._entities.Add(GetDbModel(entity));

                        // ((DbContext)_context).Entry(entity).State = EntityState.Added;
                        //this._entities.Attach(entity);

                    }
                }

                this._context.SaveChanges();
        }

        public void Update(IEnumerable<T> entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");
            this._context.SaveChanges();
        }

        public void Delete(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            //if (!this._context.IsAttached(entity))
            //    this._entities.Attach(entity);

            this._entities.Remove(entity);
            this._context.SaveChanges();
        }

        public void Delete(IEnumerable<T> entities)
        {
            if (entities == null)
                throw new ArgumentNullException("entities");


            foreach (var entity in entities)
            {
                if (entity.Id != 0)
                {
                    this._entities.Attach(entity);
                    ((DbContext)_context).Entry(entity).State = EntityState.Deleted;
                    //this._entities.Remove(entity);
                }
            }
            this._context.SaveChanges();
        }

        public virtual IQueryable<T> Table => _entities;

        //TODO implement IDisposable interface
    }

    public partial interface IRepository<T> where T : BaseEntity
    {
        T GetById(object id);
        void Insert(T entity);
        void Insert(IEnumerable<T> enities);
        void Update(T entity);
        void UpdateAndAttach(T entity, bool delete = false);
        void UpdateAndAttach(IEnumerable<T> entities, bool delete = false);
        void Update(IEnumerable<T> enities);
        void Delete(T entity);
        void Delete(IEnumerable<T> enities);
        IQueryable<T> Table { get; }
    }
}