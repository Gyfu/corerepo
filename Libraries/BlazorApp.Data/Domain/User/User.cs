﻿using System;
using BlazorApp.Core;

namespace BlazorApp.Data.Domain.User
{
    public class User : BaseStatusEntity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public DateTime BirthDay { get; set; }
        public string Login { get; set; }

    }
}
