﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using BlazorApp.Core;

namespace BlazorApp.Services
{
    internal class ConditionException : Exception
    {
        public ConditionException(string message) : base(message) { }
    }

    internal class TransitionException : Exception
    {
        public TransitionException(string message) : base(message) { }
    }

    public sealed class StateService : IStateService
    {

        public enum State
        {
            [Description("Нет")]
            None = 0,

            [Description("Создан|Создана|Создано|Созданы")]
            New = 1,

            [Description("На редактировании")]
            Edit = 2,

            [Description("Удален|Удалена|Удалено|Удалены")]
            Deleted = 3,

            [Description("На утверждении")]
            ToApprove = 4,

            [Description("Утвержден|Утверждена|Утверждено|Утверждены")]
            Approved = 5,

            [Description("Черновик")]
            Draft = 6,

            [Description("Архив")]
            Archive = 7,

        }

        public enum ObjectType
        {
            None,
            User
        }

        public delegate void OnTransition(BaseStatusEntity obj, string user);
        public delegate bool OnCondition(BaseStatusEntity obj, string user);

        private readonly IList<States> _states;
        private readonly ISystemService _systemService;

        public StateService(ISystemService systemService)
        {
            _systemService = systemService;
            _states = new List<States>
            {
                states_for(ObjectType.None).State(State.None,
                    transition(State.New, onTransition: OnNew),
                    transition(State.Deleted, onTransition: OnDelete)).State(State.New,
                    transition(State.Deleted, onTransition: OnDelete)).State(State.Deleted,
                    transition(State.New, onTransition: OnNew)),
                states_for(ObjectType.User).State(State.None,
                    transition(State.New, onTransition: OnNew),
                    transition(State.Deleted, onTransition: OnDelete)).State(State.New,
                    transition(State.Deleted, onTransition: OnDelete)).State(State.Deleted,
                    transition(State.New, onTransition: OnNew)),
            };
        }


        private void OnState(BaseStatusEntity obj, State state, string user)
        {
            _systemService.InsertLog(GetObjectType(obj), obj.Id, "StatusID", "Статус", GetStateName(obj), GetStateName(state), user);
            obj.StatusId = (int)state;
        }


        private void OnNew(BaseStatusEntity obj, string user)
        {
            _systemService.InsertLog(GetObjectType(obj), obj.Id, "StatusID", "Статус", GetStateName(obj), GetStateName(State.New), user);
            obj.StatusId = (int)State.New;
        }
        

        private void OnDelete(BaseStatusEntity obj, string user)
        {
            _systemService.InsertLog(GetObjectType(obj), obj.Id, "StatusID", "Статус", GetStateName(obj), GetStateName(State.Deleted), user);
            obj.StatusId = (int)State.Deleted;
        }
        private void OnEdit(BaseStatusEntity obj, string user)
        {
            _systemService.InsertLog(GetObjectType(obj), obj.Id, "StatusID", "Статус", GetStateName(obj), GetStateName(State.Edit), user);
            obj.StatusId = (int)State.Edit;
        }
        private void OnDraft(BaseStatusEntity obj, string user)
        {
            _systemService.InsertLog(GetObjectType(obj), obj.Id, "StatusID", "Статус", GetStateName(obj), GetStateName(State.Draft), user);
            obj.StatusId = (int)State.Draft;
        }
        private void OnArchive(BaseStatusEntity obj, string user)
        {
            _systemService.InsertLog(GetObjectType(obj), obj.Id, "StatusID", "Статус", GetStateName(obj), GetStateName(State.Archive), user);
            obj.StatusId = (int)State.Archive;
        }

        private Transition transition(State target, OnTransition onTransition = null, OnCondition condition = null, OnTransition beforeTransition = null, OnTransition after_transition = null)
        {
            return new Transition(target, onTransition: onTransition, onCondition: condition, beforeTransition: beforeTransition, afterTransition: after_transition);
        }

        private static States states_for(ObjectType type)
        {
            return new States(type);
        }

        public static State GetCurrentState(BaseStatusEntity obj)
        {
            return (State)obj.StatusId;
        }

        public static State GetCurrentState(long state)
        {
            return (State)state;
        }

        public static string GetStateName(BaseStatusEntity obj, int gen = 0)
        {
            return GetStateName((State)obj.StatusId, gen);
        }


        public static string GetOtherStateName<T>(long state)
        {
            return GetOtherStateName((T)Enum.Parse(typeof(T),state.ToString(),true));
        }
        
        public static string GetOtherStateName<T>(T state)
        {
            var stateName = "Неизвестно";
            var field = state.GetType().GetField(state.ToString());
            if (field != null)
            {
                var attributes = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attributes.Length > 0)
                    stateName = attributes[0].Description;
            }
            return stateName;
        }

        public static string GetStateName(long state, int gen = 0)
        {
            return GetStateName((State)state, gen);
        }

        public static string GetStateName(State state, int gen = 0)
        {
            var stateName = "Неизвестно";
            var field = state.GetType().GetField(state.ToString());
            if (field != null)
            {
                var attributes = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attributes.Length > 0)
                {
                    var names = attributes[0].Description.Split('|');
                    stateName = names.Length > gen ? names[gen] : names[0];
                }
            }
            return stateName;
        }


        public static string GetObjectType(BaseStatusEntity obj)
        {
            foreach (var type in Enum.GetNames(typeof(ObjectType)))
            {
                var str = obj.GetType().Name;
                if (obj.GetType().Name.StartsWith(type)) return type;
            }
            return obj.GetType().Name.Split('_')[0];
        }

        public void AddBeforeTransition(ObjectType type, State from, State to, OnTransition transition)
        {
            var transitions = _states.FirstOrDefault(s => s.GetForType() == type)?.GetTransitions(from).Where(tr => tr.GetState() == to);

            if (transitions == null) return;
            {
                foreach (var tr in transitions)
                {
                    tr.AddBeforeTransition(transition);
                }
            }
        }

        public void AddBeforeTransition(ObjectType type, OnTransition transition)
        {
            var states = _states.Where(s => s.GetForType() == type);

            foreach (var state in states)
            {
                var transitions = state.GetTransitions();
                foreach (var tr in transitions)
                {
                    tr.AddBeforeTransition(transition);
                }
            }
        }

        public void AddAfterTransition(ObjectType type, State from, State to, OnTransition transition)
        {
            var transitions = _states.FirstOrDefault(s => s.GetForType() == type)?.GetTransitions(from).Where(tr => tr.GetState() == to);

            if (transitions == null) return;
            {
                foreach (var tr in transitions)
                {
                    tr.AddAfterTransition(transition);
                }
            }
        }

        public void AddAfterTransition(ObjectType type, OnTransition transition)
        {
            var states = _states.Where(s => s.GetForType() == type);

            foreach (var state in states)
            {
                var transitions = state.GetTransitions();
                foreach (var tr in transitions)
                {
                    tr.AddAfterTransition(transition);
                }
            }
        }

        public void AddCondition(ObjectType type, State from, State to, OnCondition condition)
        {
            var transitions = _states.FirstOrDefault(s => s.GetForType() == type)?.GetTransitions(from).Where(tr => tr.GetState() == to);

            if (transitions == null) return;
            {
                foreach (var tr in transitions)
                {
                    tr.AddCondition(condition);
                }
            }
        }

        private class States
        {
            private ObjectType _type;
            private IDictionary<State, IList<Transition>> _transitions;

            public ObjectType GetForType()
            {
                return _type;
            }

            public States(ObjectType type)
            {
                _transitions = new Dictionary<State, IList<Transition>>();
                _type = type;
            }

            public IList<Transition> GetTransitions(State state)
            {
                IList<Transition> transitionsList;
                if (!_transitions.TryGetValue(state, out transitionsList))
                {
                    transitionsList = new List<Transition>();
                }

                return transitionsList;
            }

            public IList<Transition> GetTransitions()
            {
                IList<Transition> transitionsList = new List<Transition>();

                foreach (var transitions in _transitions)
                {
                    foreach (var transition in transitions.Value)
                    {
                        transitionsList.Add(transition);
                    }
                }

                return transitionsList;
            }

            public States State(State state, params Transition[] transitions)
            {
                IList<Transition> transitionsList = new List<Transition>();
                if (_transitions.ContainsKey(state))
                {
                    _transitions[state].Concat(transitionsList);
                }
                else
                {
                    foreach (var transition in transitions)
                    {
                        transitionsList.Add(transition);
                    }
                    _transitions.Add(state, transitionsList);
                }

                return this;
            }
        }

        private class Transition
        {
            private readonly OnTransition _onTransition;
            private OnTransition _onBeforeTransition;
            private OnTransition _onAfterTransition;
            private OnCondition _onCondition;

            private State _target;

            public State GetState()
            {
                return _target;
            }

            public Transition(State target, OnTransition onTransition, OnCondition onCondition, OnTransition beforeTransition, OnTransition afterTransition)
            {
                _target = target;
                if (onTransition != null)
                {
                    _onTransition += onTransition;
                }
                if (onCondition != null)
                {
                    _onCondition += onCondition;
                }
                if (beforeTransition != null)
                {
                    _onBeforeTransition += beforeTransition;
                }
                if (afterTransition != null)
                {
                    _onAfterTransition += afterTransition;
                }
            }

            public void AddBeforeTransition(OnTransition onTransition)
            {
                _onBeforeTransition += onTransition;
            }

            public void AddAfterTransition(OnTransition onTransition)
            {
                _onAfterTransition += onTransition;
            }

            public void AddCondition(OnCondition onCondition)
            {
                _onCondition += onCondition;
            }

            public void Run(BaseStatusEntity obj, string user)
            {
                var result = true;
                if (_onCondition != null)
                {
                    result = _onCondition(obj, user);
                }

                if (result)
                {
                    if (_onBeforeTransition != null)
                    {
                        try
                        {
                            _onBeforeTransition(obj, user);
                        }
                        catch (Exception e)
                        {
                            throw new TransitionException("Before transition error: " + e.Message);
                        }
                    }
                    if (_onTransition != null)
                    {
                        try
                        {
                            _onTransition(obj, user);
                        }
                        catch (Exception e)
                        {
                            throw new TransitionException("Transition error: " + e.Message);
                        }
                    }
                    if (_onAfterTransition != null)
                    {
                        try
                        {
                            _onAfterTransition(obj, user);
                        }
                        catch (Exception e)
                        {
                            throw new TransitionException("After transition error: " + e.Message);
                        }
                    }
                }
                else
                {
                    throw new ConditionException("Condition was failed");
                }
            }
        }

        public bool CheckStatus(State state, BaseStatusEntity obj)
        {
            var currentState = (State)obj.StatusId;
            var type = _states.FirstOrDefault(s => obj.GetType().Name.StartsWith(s.GetForType().ToString()));

            IEnumerable<Transition> transitions;
            if (type != null)
            {

                transitions = type.GetTransitions(currentState).Where(tr => tr.GetState() == state);
            }
            else
            {
                transitions = _states.FirstOrDefault(s => s.GetForType() == ObjectType.None)?.GetTransitions(currentState).Where(tr => tr.GetState() == state);
            }

            if (transitions!= null && !transitions.Any())
            {
                transitions = _states.FirstOrDefault(s => s.GetForType() == ObjectType.None)?.GetTransitions(currentState).Where(tr => tr.GetState() == state);
                if (transitions != null && !transitions.Any())
                {
                    return false;
                }
            }
            return true;
        }

        public IEnumerable<State> GetAvaliableStates(BaseStatusEntity obj)
        {
            var currentState = (State)obj.StatusId;
            var type = _states.FirstOrDefault(s => obj.GetType().Name.StartsWith(s.GetForType().ToString()));

            IEnumerable<State> states = null;
            if (type != null)
            {

                states = type.GetTransitions(currentState).Select(tr => tr.GetState());
            }

            if (states == null || !states.Any())
            {
                states = _states.FirstOrDefault(s => s.GetForType() == ObjectType.None).GetTransitions(currentState).Select(tr => tr.GetState());
            }
            return states;
        }

        public void ToState(State state, BaseStatusEntity obj, string user)
        {
            var currentState = (State)obj.StatusId;

            //if (currentState == null)
            //{
            //    currentState = State.None;
            //}

            var type = _states.FirstOrDefault(s => obj.GetType().Name == s.GetForType().ToString() || obj.GetType().BaseType.Name == s.GetForType().ToString());

            IEnumerable<Transition> transitions;
            if (type != null)
            {

                transitions = type.GetTransitions(currentState).Where(tr => tr.GetState() == state);
            }
            else
            {
                transitions = _states.FirstOrDefault(s => s.GetForType() == ObjectType.None)?.GetTransitions(currentState).Where(tr => tr.GetState() == state);
            }

            if (transitions != null && !transitions.Any())
            {
                transitions = _states.FirstOrDefault(s => s.GetForType() == ObjectType.None)?.GetTransitions(currentState).Where(tr => tr.GetState() == state);
                if (transitions != null && !transitions.Any())
                {
                    throw new TransitionException("Transition not found");
                }
            }

            foreach (var transition in transitions)
            {
                transition.Run(obj, user);
            }
        }
    }

    public partial interface IStateService
    {
        IEnumerable<StateService.State> GetAvaliableStates(BaseStatusEntity obj);
        void ToState(StateService.State state, BaseStatusEntity obj, string user);
        void AddBeforeTransition(StateService.ObjectType type, StateService.State from, StateService.State to, StateService.OnTransition transition);
        void AddBeforeTransition(StateService.ObjectType type, StateService.OnTransition transition);
        void AddAfterTransition(StateService.ObjectType type, StateService.State from, StateService.State to, StateService.OnTransition transition);
        void AddAfterTransition(StateService.ObjectType type, StateService.OnTransition transition);
        void AddCondition(StateService.ObjectType type, StateService.State from, StateService.State to, StateService.OnCondition condition);
        bool CheckStatus(StateService.State state, BaseStatusEntity obj);
    }
}
