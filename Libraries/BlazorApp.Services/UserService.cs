﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorApp.Data;
using BlazorApp.Data.Domain.User;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace BlazorApp.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _userRepository;
        private readonly ISystemService _systemService;
        private readonly IStateService _stateService;
        private readonly string _login;

        public UserService(IRepository<User> userRepository, IHttpContextAccessor contextAccessor, ISystemService systemService, IStateService stateService)
        {
            _userRepository = userRepository;
            _systemService = systemService;
            _stateService = stateService;
            _login = contextAccessor.HttpContext.User.Identity.Name;
        }

        public async Task<List<User>> GetUsers()
        {
            return await _userRepository.Table.Where(t=>t.StatusId == (int)StateService.State.New).ToListAsync();
        }

        public void InsertUser(User user)
        {
            //user.StatusId = (int)StateService.State.New;
            _userRepository.Insert(user);
            //_systemService.InsertLog("User", user.Id, "StatusId", "Статус", StateService.GetStateName(StateService.State.None), StateService.GetStateName(StateService.State.New), _login);
            _stateService.ToState(StateService.State.New, user, _login);
            _userRepository.Update(user);
        }
    }

    public interface IUserService
    {
        Task<List<User>> GetUsers();
        void InsertUser(User user);
    }
}
