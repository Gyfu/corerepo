﻿using System;
using System.Collections.Generic;
using System.Linq;
using BlazorApp.Data;
using BlazorApp.Data.Domain;
using BlazorApp.Data.Domain.System;
using BlazorApp.Data.Domain.User;
using Microsoft.EntityFrameworkCore;

namespace BlazorApp.Services
{
    public partial class SystemService : ISystemService
    {
        private readonly IRepository<Log> _logRepository;
        private readonly IRepository<User> _userRepository;

        public SystemService(IRepository<Log> logRepository, IRepository<User> userRepository)
        {
            _logRepository = logRepository;
            _userRepository = userRepository;
        }
        public bool VerifyMD5(byte[] hash, byte[] newHash)
        {
            bool result = false;

            if (hash != null && hash.Length == newHash.Length)
            {
                int i = 0;
                while ((i < newHash.Length) && (hash[i] == newHash[i]))
                {
                    ++i;
                }
                if (i == newHash.Length)
                {
                    result = true;
                }
            }

            return result;
        }
        public IList<Log> GetLogByID(long id)
        {
            IQueryable<Log> query = _logRepository.Table;
            query = query.Where(t => t.Id == id);
            return query.ToList();
        }

        public virtual IList<Log> GetLog(long ObjectId, string Type, string PropName)
        {
            IQueryable<Log> query = _logRepository.Table.Include(t=>t.User);

            query = query.OrderByDescending(t => t.Id);
            query = query.Where(t => t.ObjectID == ObjectId && t.Type == Type);
            if (Type == "TechDocument")
                query = query.Where(t => t.PropValueNew != "Открыт" && !t.PropDisplayName.Contains("материал"));
            if (!String.IsNullOrEmpty(PropName))
                query = query.Where(t => t.PropName == PropName);

            return query.ToList();
        }

        public virtual IList<Log> GetLogByType(string Type)
        {
            IQueryable<Log> query = _logRepository.Table.Include(t => t.User);

            query = query.Where(t => t.Type == Type).OrderByDescending(t => t.Created);

            return query.ToList();
        }

        private void InsertLog(Log Log)
        {
            if (Log == null)
                throw new ArgumentNullException("Log");

            _logRepository.Insert(Log);
        }

        public void InsertLog(string objectType, long objectID, string propValue, string user)
        {
            long id = 0;
            User currentUser;
            if (long.TryParse(user, out id))
            {
                currentUser = _userRepository.Table.FirstOrDefault(u => u.Id == id);
            }
            else
            {
                currentUser = _userRepository.Table.FirstOrDefault(u => u.Login == user);
            }
            //var users = _userRepository.Table.ToList();
            //var currentUser = users.FirstOrDefault(t => t.Login == user.Replace("ISTOK\\", ""));
            Log log = new Log()
            {
                //ObjectGUID = item.GUID,
                Type = objectType,
                ObjectID = objectID,
                PropDisplayName = "Статус",
                PropName = "StatusID",
                PropValueOld = "",
                PropValueNew = propValue,
                Created = DateTime.Now,
                CreatedBy = currentUser!=null ? currentUser.Id : 0
            };

            this.InsertLog(log);
        }

        public void InsertLog(string objectType, long objectID,
            string propName, string propDisplayName, string propValueOld, string propValueNew, string user)
        {
            long id = 0;
            User currentUser;
            if (long.TryParse(user, out id))
            {
                currentUser = _userRepository.Table.FirstOrDefault(u => u.Id == id);
            }
            else
            {
                currentUser = _userRepository.Table.FirstOrDefault(u => u.Login == user);
            }
            //var users = _userRepository.Table.ToList();
            //var currentUser = users.FirstOrDefault(t => t.Login == user.Replace("ISTOK\\", ""));
            Log log = new Log()
            {
                Type = objectType,
                ObjectID = objectID,
                PropDisplayName = propDisplayName,
                PropName = propName,
                PropValueOld = propValueOld,
                PropValueNew = propValueNew,
                Created = DateTime.Now,
                CreatedBy = currentUser != null ? currentUser.Id : 0
            };

            this.InsertLog(log);
        }


        public void InsertLog(string objectType, long objectID,
            string propName, string propDisplayName, string propValueOld, string propValueNew, string user, string comment)
        {
            long id = 0;
            User currentUser;
            if (long.TryParse(user, out id))
            {
                currentUser = _userRepository.Table.FirstOrDefault(u => u.Id == id);
            }
            else
            {
                currentUser = _userRepository.Table.FirstOrDefault(u => u.Login == user);
            }
            //var users = _userRepository.Table.ToList();
            //var currentUser = users.FirstOrDefault(t => t.Login == user.Replace("ISTOK\\", ""));
            Log log = new Log()
            {
                Type = objectType,
                ObjectID = objectID,
                PropDisplayName = propDisplayName,
                PropName = propName,
                PropValueOld = propValueOld,
                PropValueNew = propValueNew,
                Created = DateTime.Now,
                CreatedBy = currentUser != null ? currentUser.Id : 0,
                Comment = comment
            };

            this.InsertLog(log);
        }
    }

    public partial interface ISystemService
    {
        IList<Log> GetLog(long ObjectId, string Type, string PropName);

        IList<Log> GetLogByID(long id);
        IList<Log> GetLogByType(string Type); 
        
        void InsertLog(string objectType, long objectID, string propValue, string user);

        void InsertLog(string objectType, long objectID, 
            string propName, string propDisplayName, string propValueOld, string propValueNew, string user);

        void InsertLog(string objectType, long objectID, 
            string propName, string propDisplayName, string propValueOld, string propValueNew, string user,
            string comment);

        //bool VerifyMD5(object mD5, byte[] hash);
        bool VerifyMD5(byte[] hash, byte[] newHash);
    }
}
