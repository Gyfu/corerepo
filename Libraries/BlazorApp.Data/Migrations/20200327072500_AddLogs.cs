﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BlazorApp.Data.Migrations
{
    public partial class AddLogs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Login",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Logs",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Created = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<long>(nullable: false),
                    PropName = table.Column<string>(nullable: true),
                    PropDisplayName = table.Column<string>(nullable: true),
                    PropValueOld = table.Column<string>(nullable: true),
                    PropValueNew = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    ObjectID = table.Column<long>(nullable: false),
                    LinkObjectType = table.Column<string>(nullable: true),
                    LinkObjectID = table.Column<long>(nullable: true),
                    Comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Logs_Users_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Logs_CreatedBy",
                table: "Logs",
                column: "CreatedBy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Logs");

            migrationBuilder.DropColumn(
                name: "Login",
                table: "Users");
        }
    }
}
