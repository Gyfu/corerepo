﻿namespace BlazorApp.Core
{
    public class BaseStatusEntity : BaseEntity
    {
        public int StatusId { get; set; }
    }
}
