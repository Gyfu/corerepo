﻿namespace BlazorApp.Core
{
    public class BaseEntity
    {
        public long Id { get; set; }
    }
}
