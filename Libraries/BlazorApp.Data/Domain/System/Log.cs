﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using BlazorApp.Core;

namespace BlazorApp.Data.Domain.System
{
    public partial class Log : BaseEntity
    {
        public virtual DateTime Created { get; set; }

        public virtual long CreatedBy { get; set; }

        public virtual string PropName { get; set; }

        public virtual string PropDisplayName { get; set; }
        
        public virtual string PropValueOld { get; set; }

        public virtual string PropValueNew { get; set; }

        //public virtual Guid ObjectGUID { get; set; }

        public virtual string Type { get; set; }

        public virtual long ObjectID { get; set; }

        public virtual string Comment { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual User.User User { get; set; }
    }
}